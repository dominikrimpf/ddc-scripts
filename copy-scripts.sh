#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

cp set-brightness /usr/local/bin/
cp switch-display-* /usr/local/bin/
